package com.example.ventanas;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.View;

public class MainActivity extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }
    public void desayunos(View vista){
    	Intent vd = new Intent(this, Desayunos.class);
    	startActivity(vd);
    	
    }
    public void bebidas(View vista){
    	Intent vd = new Intent(this, Bebidas.class);
    	startActivity(vd);
    	
    }
    public void almuerzos(View vista){
    	Intent vd = new Intent(this, Almuerzos.class);
    	startActivity(vd);
    	
    }
    public void platosespeciales(View vista){
    	Intent vd = new Intent(this, Platosespeciales.class);
    	startActivity(vd);
    	
    }
    public void postres(View vista){
    	Intent vd = new Intent(this, Postres.class);
    	startActivity(vd);
    	
    }
    public void finalizar(View vista){
    	
    	finish();//Esta instruccion finaliza el hilo, cada vista debe tener una de estas
    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }
    
}
