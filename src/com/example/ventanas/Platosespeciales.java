package com.example.ventanas;

import android.os.Bundle;
import android.app.Activity;
import android.view.Menu;
import android.view.View;

public class Platosespeciales extends Activity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_platosespeciales);
	}
	public void finalizar(View vista){
		finish();
	}
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.platosespeciales, menu);
		return true;
	}

}
